﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SurveySolutions
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            treeView1.Nodes.Clear();

            var SuSo = new SurveySolutions.ApiV1("https://demo.mysurvey.solutions", "Expapi", "ExpApi2000");
            var supervisors = SuSo.GetSupervisorsAll();
            foreach (var s in supervisors)
            {
                var n = treeView1.Nodes.Add(s.UserName);

                var interviewers = SuSo.GetInterviewersOfTeam(s.UserId);
                foreach (var i in interviewers.Users)
                    n.Nodes.Add(i.UserName);
            }

            var interv = SuSo.GetInterviewsAll("ef676e10c4a442039968656acfb2a8c3", 1);
            foreach (var i in interv)
            {
                listBox1.Items.Add(i.InterviewId);
            }
        }

    }
}
