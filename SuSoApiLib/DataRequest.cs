﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SurveySolutions;

namespace SurveySolutions
{
    public class DataRequest
    {
        public string Template;
        public int Version;
        public DataType Fmt;

        public string SurveyHandle
        {
            get
            {
                return Template + "$" + Version.ToString();
            }
        }

        public string OnServer(string server)
        {
            return string.Format("{0}{3}export/{1}/{2}/",
                server, Fmt.ToString(), SurveyHandle, ApiV1.ApiPoint);
        }
    }
}
