﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;

using Newtonsoft.Json;

namespace SurveySolutions
{
    public abstract class ApiCore
    {
        internal string _password;
        internal string _user;
        internal string _server;

        public ApiCore(string server, string user, string password)
        {
            _server = server;
            _user = user;
            _password = password;
        }

        internal string GetCredentials64()
        {
            var creds = String.Format("{0}:{1}", _user, _password);
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(creds));
        }

        internal HttpClient GetClient()
        {
            var authValue = new AuthenticationHeaderValue("Basic", GetCredentials64());
            var client = new HttpClient()
            {
                DefaultRequestHeaders = { Authorization = authValue }
            };

            //client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip,deflate"); // this causes the final file to be compressed! avoid
            return client;
        }

        internal string ApiPost(Uri url)
        {
            using (var httpClient = GetClient())
            {
                httpClient.BaseAddress = url;
                using (var content = new StringContent(String.Empty, Encoding.Default, "application/json"))
                using (var response = httpClient.PostAsync(String.Empty, content).Result)
                    return response.Content.ReadAsStringAsync().Result;
            }
        }

        internal string ApiGet(Uri url)
        {
            using (var httpClient = GetClient())
            {
                var z = new HttpRequestMessage(HttpMethod.Get, url);
                var res = httpClient.SendAsync(z).Result;
                if (res.StatusCode == System.Net.HttpStatusCode.OK)
                    return res.Content.ReadAsStringAsync().Result; // jsonTxt
                throw new Exception("Survey Solutions API: Error while downloading");
            }
        }

        internal T ApiGet<T>(Uri url) {
            var response = ApiGet(url);
            return JsonConvert.DeserializeObject<T>(response);
        }

        public string ApiPatch(Uri requestUri)
        {
            HttpContent content = new StringContent(String.Empty, Encoding.Default, "application/json");
            var cancellationToken = new CancellationToken();
            var client = GetClient();
            var method = new HttpMethod("PATCH");
            var request = new HttpRequestMessage(method, requestUri)
            {
                Content = content
            };

            var response = client.SendAsync(request, cancellationToken);
            return response.Result.Content.ReadAsStringAsync().Result;
        }
    }
}
