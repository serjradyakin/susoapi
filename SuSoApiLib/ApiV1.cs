﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using Newtonsoft.Json;

namespace SurveySolutions
{
    // API SYNTAX:
    // https://demo.mysurvey.solutions/apidocs/index#/

    public class ApiV1: ApiCore
    {
        public ApiV1(string server, string user, string password) : base(server, user, password) { }

        const int WaitPeriod = 500;
        const int RollPageSize = 40;
        public const string ApiPoint = "/api/v1/";

        public Uri ServerApiPoint()
        {
            return new Uri(new Uri(_server), ApiPoint);
        }

        #region ------------------ EXPORT ----------------------------
        // https://demo.mysurvey.solutions/api/v1/export/format/guid$version/details    (GET)
        // https://demo.mysurvey.solutions/api/v1/export/format/guid$version/start      (POST)
        // https://demo.mysurvey.solutions/api/v1/export/format/guid$version/           (GET)
        
        public void ExportRefresh(DataRequest data)
        {
            using (var httpClient = GetClient())
            {
                httpClient.BaseAddress = new Uri(String.Format("{0}{1}", data.OnServer(_server), "start/"));
                using (var content = new StringContent(String.Empty, Encoding.Default, "application/json"))  // for each GET request and for some POSTs where result is important
                {
                    var z = new HttpRequestMessage(HttpMethod.Post, httpClient.BaseAddress);
                    using (var response = httpClient.PostAsync(String.Empty, content).Result)
                    {
                        var responseData = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine(response.StatusCode);
                    }
                }
            }
        }

        public void ExportCancel(DataRequest data)
        {
            using (var httpClient = GetClient())
            {
                httpClient.BaseAddress = new Uri(String.Format("{0}{1}", data.OnServer(_server), "cancel/"));
                using (var content = new StringContent(String.Empty, Encoding.Default, "application/json"))  // for each GET request and for some POSTs where result is important
                {
                    var z = new HttpRequestMessage(HttpMethod.Post, httpClient.BaseAddress);
                    using (var response = httpClient.PostAsync(String.Empty, content).Result)
                    {
                        var responseData = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine(response.StatusCode);
                    }
                }
            }
        }
        
        public Details ExportDetails(DataRequest data)
        {
            using (var httpClient = GetClient())
            {
                // https://demo.mysurvey.solutions/api/v1/export/format/guid$version/details
                var url = new Uri(new Uri(data.OnServer(_server)), "details/");

                var z = new HttpRequestMessage(HttpMethod.Get, url);
                var res = httpClient.SendAsync(z).Result;
                if (res.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var jsonTxt = res.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<Details>(jsonTxt);
                }
                throw new Exception("error while downloading");
            }
        }

        public Details WaitForDataReady(DataRequest data)
        {
            var details = ExportDetails(data);
            while (details.ExportStatus == Status.Running.ToString()
                || details.ExportStatus == Status.Compressing.ToString()
                || details.ExportStatus == Status.Queued.ToString())
            {
                Thread.Sleep(WaitPeriod);
                details = ExportDetails(data); // repeat
                if (details.RunningProcess != null)
                {
                    Console.WriteLine(string.Format("Running since: {0}, currently done: {1} percent",
                        details.RunningProcess.StartDate,
                        details.RunningProcess.ProgressInPercents));
                }
                else
                {
                    Console.WriteLine("-");
                }
            }
            return details;
        }

        public void DownloadDataAsFile(DataRequest data, string filename)
        {
            using (var httpClient = GetClient())
            {
                using (var response = httpClient.GetByteArrayAsync(data.OnServer(_server)))
                {
                    response.Wait();
                    File.WriteAllBytes(filename, response.Result);
                }
            }
        }

        #endregion

        #region ------------------ USERS -----------------------------
        
        /// <summary>
        /// Get list of supervisors
        /// </summary>
        /// <returns>List of supervisors</returns>
        public UserApiView GetSupervisors(int limit = 40, int offset = 1)
        {
            // todo: roll this
            // https://demo.mysurvey.solutions/api/v1/supervisors
            var url = new Uri(ServerApiPoint(), "supervisors/?limit=" + limit.ToString() + "&offset=" + offset.ToString());
            return ApiGet<UserApiView>(url);
        }

        public List<UserApiItem> GetSupervisorsAll()
        {
            var o = 1;  // should the paging start with 0 or 1?
            var result = new List<UserApiItem>();

            UserApiView ri;
            do
            {
                ri = GetSupervisors(RollPageSize, o);
                foreach (var i in ri.Users)
                    result.Add(i);
                o++;
            }
            while (ri.TotalCount > result.Count);
            return result;
        }

        /// <summary>
        /// Get list of the interviewers of a particular team
        /// </summary>
        /// <param name="SupervisorGuid">GUID of the team supervisor</param>
        /// <returns>List of interviewers in the requested team</returns>
        public UserApiView GetInterviewersOfTeam(string SupervisorGuid)
        {
            // todo: roll this
            // https://demo.mysurvey.solutions/api/v1/supervisors/{supervisorid}/interviewers
            var url = new Uri(ServerApiPoint(), "supervisors/" + SupervisorGuid + "/interviewers");
            return ApiGet<UserApiView>(url);
        }

        /// <summary>
        /// Get detailed info about a single supervisor
        /// </summary>
        /// <param name="UserGuid">GUID of the supervisor</param>
        /// <returns>Detailed information about the supervisor</returns>
        public UserApiDetails GetSupervisorDetails(string SupervisorGuid)
        {
            // https://demo.mysurvey.solutions/api/v1/supervisors/{supervisorid}
            var url = new Uri(ServerApiPoint(), "supervisors/" + SupervisorGuid);
            return ApiGet<UserApiDetails>(url);
        }

        /// <summary>
        /// Get detailed info about a single interviewer
        /// </summary>
        /// <param name="InterviewerGuid">GUID of the interviewer</param>
        /// <returns>Detailed information about the interviewer</returns>
        public UserApiDetails GetInterviewerDetails(string InterviewerGuid)
        {
            // https://demo.mysurvey.solutions/api/v1/interviewers/{interviewerid}
            var url = new Uri(ServerApiPoint(), "interviewers/" + InterviewerGuid);
            return ApiGet<UserApiDetails>(url);
        }

        /// <summary>
        /// Get detailed info about a single user
        /// </summary>
        /// <param name="InterviewerGuid">GUID of the user</param>
        /// <returns>Detailed information about the user</returns>
        public UserApiDetails GetUserDetails(string UserGuid)
        {
            // https://demo.mysurvey.solutions/api/v1/users/{userid}
            var url = new Uri(ServerApiPoint(), "users/" + UserGuid);
            return ApiGet<UserApiDetails>(url);
        }

        // currently no way to obtain the list of HQ users

        /// <summary>
        /// Archive a user
        /// </summary>
        /// <param name="UserGuid">GUID of the user to be archived.</param>
        public void UserArchive(string UserGuid)
        {
            // https://demo.mysurvey.solutions/api/v1/users/{userid}/archive
            var url = new Uri(ServerApiPoint(), "users/" + UserGuid + "/archive");
            ApiPatch(url);
        }

        /// <summary>
        /// Unarchive a user
        /// </summary>
        /// <param name="UserGuid">GUID of the user to be unarchived.</param>
        public void UserUnarchive(string UserGuid)
        {
            // https://demo.mysurvey.solutions/api/v1/users/{userid}/archive
            var url = new Uri(ServerApiPoint(), "users/" + UserGuid + "/archive");
            ApiPatch(url);
        }


#endregion

        #region -------------------QUESTIONNAIRES---------------------

        public QuestionnaireApiView GetQuestionnaires(int limit=40, int offset=1)
        {
            // https://demo.mysurvey.solutions/api/v1/questionnaires
            var url = new Uri(ServerApiPoint(), "questionnaires/?limit=" + limit.ToString() + "&offset=" + offset.ToString());
            return ApiGet<QuestionnaireApiView>(url);
        }

        public List<QuestionnaireApiItem> GetQuestionnairesAll()
        {
            // todo: in a similar way collect all other large lists
            var o = 1;  // should the paging start with 0 or 1?
            var result = new List<QuestionnaireApiItem>();

            QuestionnaireApiView ri;
            do
            {
                ri = GetQuestionnaires(RollPageSize, o);
                foreach (var i in ri.Questionnaires)
                    result.Add(i);
                o++;
            }
            while (ri.TotalCount > result.Count);
            return result;
        }

        // THIS DOESN'T SEEM TO WORK: GET /api/v1/questionnaires/{id}/{version}

        public String GetQuestionnaireDocument(string QuestionnaireId, int Version)
        {
            var url = new Uri(ServerApiPoint(), "questionnaires/" + QuestionnaireId + "/" + Version.ToString() + "/document");
            return ApiGet<string>(url);
        }

        public InterviewApiView GetInterviews(string QuestionnaireId, int Version, int limit = 40, int offset = 1)
        {
            // https://demo.mysurvey.solutions/api/v1/questionnaires
            var url = new Uri(ServerApiPoint(),
                "questionnaires/" + QuestionnaireId 
                + "/" + Version.ToString() 
                + "/interviews/?limit=" + limit.ToString() 
                + "&offset=" + offset.ToString());
            return ApiGet<InterviewApiView>(url);
        }

        public List<InterviewApiItem> GetInterviewsAll(string QuestionnaireId, int Version)
        {
            // todo: in a similar way collect all other large lists
            var o = 1; // should the paging start with 0 or 1?
            var result = new List<InterviewApiItem>();
            InterviewApiView ri;
            do
            {
                ri = GetInterviews(QuestionnaireId, Version, RollPageSize, o);
                foreach (var i in ri.Interviews)
                    result.Add(i);
                o++;
            } while (ri.TotalCount > result.Count);
            
            return result;
        }

        #endregion

        #region -----------------INTERVIEWS---------------------------

        /// <summary>
        /// Leave a comment on a question using questionnaire variable name and roster vector
        /// </summary>
        /// <param name="InterviewGuid">GUID of the interview</param>
        /// <param name="Varname">Name of the variable</param>
        /// <param name="RosterVector">[NOT SUPPORTED YET] Roster address</param>
        /// <param name="Comment">Text of the comment</param>
        public void PostCommentByVariable(string InterviewGuid, string Varname, int[] RosterVector, string Comment)
        {
            if (RosterVector != null)
                throw new NotImplementedException("RosterVector is not supported yet");

            var url = String.Format(
                "{0}interviews/{1}/comment-by-variable/{2}?comment={3}",
                ServerApiPoint(), InterviewGuid, Varname, Comment);

            ApiPost(new Uri(url)); // ignore response for now
        }

        public void Approve(string InterviewGuid, string Comment = "")
        {
            ApiPatch(new Uri(ServerApiPoint(), "interviews/" + InterviewGuid + "/approve?comment=" + Comment));
        }

        
        public void Reject(string InterviewGuid, string Comment = "")
        {
            ApiPatch(new Uri(ServerApiPoint(), "interviews/" + InterviewGuid + "/reject?comment=" + Comment));
        }


        public void HqApprove(string InterviewGuid, string Comment = "")
        {
            ApiPatch(new Uri(ServerApiPoint(), "interviews/" + InterviewGuid + "/hqapprove?comment=" + Comment));
        }

        public void HqReject(string InterviewGuid, string Comment = "")
        {
            ApiPatch(new Uri(ServerApiPoint(), "interviews/" + InterviewGuid + "/hqreject?comment=" + Comment));
        }

        public void HqUnapprove(string InterviewGuid, string Comment = "")
        {
            ApiPatch(new Uri(ServerApiPoint(), "interviews/" + InterviewGuid + "/hqunapprove?comment=" + Comment));
        }
        #endregion
    }
}
