﻿using System;
using System.Collections.Generic;

namespace SurveySolutions
{
    public class UserApiDetails
    {
        public Boolean IsArchived;
        public string UserId;
        public string UserName;
        public string[] Roles;
        public Boolean IsLocked;
        public DateTime CreationDate;
        public string Email;
        public string PhoneNumber;
        public string FullName;
    }

    public class UserApiItem
    {
        public Boolean IsLocked;
        public DateTime CreationDate;
        public string UserId; // GUID
        public string UserName;
        public string Email;
        public string DeviceId;
    }

    public class UserApiView
    {
        public List<UserApiItem> Users;
        public string Order;
        public int Limit;
        public int TotalCount;
        public int Offset;
    }

    public enum DataType
    {
        tabular, spss, stata, binary, paradata
    }

    public enum Status
    {
        NotStarted, Queued, Running, Compressing, Finished, FinishedWithError
    }

    public class Process
    {
        public DateTime? StartDate;
        public double ProgressInPercents;
    }

    public class Details
    {
        public Boolean HasExportedFile;
        public DateTime? LastUpdateDate;
        public string ExportStatus;
        public Process RunningProcess;
    }

    // ----- questionnaire -------------------

    public class QuestionnaireApiView
    {
        public List<QuestionnaireApiItem> Questionnaires;

        public string Order;
        public int Limit;
        public int TotalCount;
        public int Offset;
    }

    public class QuestionnaireApiItem
    {
        public string QuestionnaireId;
        public int Version;
        public string Title;
        public string LastEntryDate;
    }

    public enum QuetionType { SingleOption, YesNo, MultyOption, Numeric, DateTime, GpsCoordinates, Text, AutoPropagate, TextList, QRBarcode, Multimedia, Area, Audio }

    public enum QuestStatus { Deleted, Restored, Created, SupervisorAssigned, InterviewerAssigned, ReadyForInterview, SentToCapi, Restarted, Completed, RejectedBySupervisor, ApprovedBySupervisor, RejectedByHeadquarters, ApprovedByHeadquarters }

    public class InterviewFeaturedQuestion
    {
        public string Id;
        public string Question;
        public string Answer;
        public QuetionType Type;
    }

    public class InterviewApiItem
    {
        public List<InterviewFeaturedQuestion> FeaturedQuestions;
        public string InterviewId;
        public string QuestionnaireId;
        public int QuestionnaireVersion;
        public string ResponsibleId;
        public string ResponsibleName;
        public int ErrorsCount;
        public QuestStatus Status;
        public string LastEntryDate;
    }

    public class InterviewApiView
    {
        public List<InterviewApiItem> Interviews;
        public string Order;
        public int Limit;
        public int TotalCount;
        public int Offset;
    }

}
