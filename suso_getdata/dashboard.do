set more off
clear
cd "C:\temp\unpackeddata\"
use "interview__diagnostics.dta"
graph pie , over(interview__status) ///
  note("as of `=c(current_date)' `=c(current_time)'", pos(6))
graph export "status_pie.png", as(png) width(1024) replace 


