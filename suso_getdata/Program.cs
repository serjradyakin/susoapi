﻿using System;
using System.IO;
using Ionic.Zip;
using SurveySolutions;

namespace suso_getdata
{
    class Program
    {
        static void Main(string[] args)
        {
            DoTask(args[0]);
        }

        private static void DoTask(string taskFileName)
        {
            string[] taskFile = File.ReadAllLines(taskFileName);
            string server = taskFile[0];
            string user = taskFile[1];
            string password = taskFile[2];
            string template = taskFile[3];
            int version = int.Parse(taskFile[4]);
            string directory = taskFile[5];

            string tempDataFileName = Path.GetTempFileName();

            var SuSo = new ApiV1(server, user, password);

            var data = new DataRequest()
            {
                Fmt = DataType.stata,
                Template = template,
                Version = version
            };

            var result = SuSo.ExportDetails(data);
            Console.WriteLine("The data was last generated on: " + result.LastUpdateDate); // JSON

            Console.WriteLine("Requesting re-generation of the data.");
            SuSo.ExportRefresh(data);
            Console.WriteLine("Requested.");

            Console.WriteLine("Waiting for data to be generated on the server");
            result = SuSo.WaitForDataReady(data);  // waiting cycle

            Console.WriteLine(result.ExportStatus + "," + result.HasExportedFile);

            Console.WriteLine("Requesting download");
            SuSo.DownloadDataAsFile(data, tempDataFileName);
            Console.WriteLine("---- done ----");

            Console.WriteLine("Unpacking...");
            using (var zip = new ZipFile())
            {
                using (var z = ZipFile.Read(tempDataFileName))
                {
                    z.ExtractExistingFile = ExtractExistingFileAction.OverwriteSilently;
                    z.ExtractAll(directory);
                }
            }
            File.Delete(tempDataFileName);
            Console.WriteLine("DONE!");

            //Console.ReadLine();
        }
    }
}
