﻿using System;
using System.IO;
using SurveySolutions;
using System.Text;

namespace SurveySolutions
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            var F = new Main();
            F.ShowDialog();

            return;

            DownloadData(
                "https://demo.mysurvey.solutions", 
                "Expapi", "ExpApi2000", 
                "c7eefc760a43436a96fac41c95514d21", 1, 
                DataType.spss, 
                @"C:\temp\mydownload.zip");
        }

        public static void DownloadData(
            string ServerName, string UserName, string UserPassword, 
            string QuestTemplate, int QuestVersion, DataType Fmt, 
            string FileName)
        {
            var SuSo = new ApiV1(ServerName, UserName, UserPassword);

            var quest = SuSo.GetQuestionnairesAll();
            var i = 0;
            foreach (var q in quest)
            {
                Console.WriteLine(i.ToString() + " " + q.Title);
                i++;
            }

            Console.WriteLine(quest[11].Title);

            var inters = SuSo.GetInterviewsAll(quest[11].QuestionnaireId, 2);
            /*
            foreach (var ii in inters)
            {
                Console.WriteLine(ii.InterviewId);
            }*/
            Console.ReadLine();
            return;

            //var doc = SuSo.GetQuestionnaireDocument(quest.Questionnaires[0].QuestionnaireId, 1);
            //Console.WriteLine(doc);

            Console.ReadLine();
            return;


            foreach (var q in quest)
            {
                Console.WriteLine(q.Title);
            }
            Console.ReadLine();
            return;

            var supervisors = SuSo.GetSupervisors();

            foreach (var sup in supervisors.Users)
            {
                Console.WriteLine(sup.UserName);
                var super_details = SuSo.GetUserDetails(sup.UserId);
                Console.WriteLine(super_details.Email);
                var Interviewers = SuSo.GetInterviewersOfTeam(sup.UserId);
                foreach (var inter in Interviewers.Users)
                {
                    Console.WriteLine("- " + inter.UserName);
                    var inter_details = SuSo.GetUserDetails(inter.UserId);
                    Console.WriteLine("  " + inter_details.Email);
                }
            }
            Console.ReadLine();
            return;


            var data = new DataRequest()
            {
                Template = QuestTemplate,
                Version = QuestVersion,
                Fmt = Fmt
            };

            var result = SuSo.ExportDetails(data);
            Console.WriteLine("The data was last generated on: " + result.LastUpdateDate); // JSON
            
            Console.WriteLine("Requesting re-generation of the data.");
            SuSo.ExportRefresh(data);
            Console.WriteLine("Requested.");
            
            Console.WriteLine("Waiting for dat to be generated on the server");
            result = SuSo.WaitForDataReady(data);  // waiting cycle

            Console.WriteLine(result.ExportStatus + "," + result.HasExportedFile);

            Console.WriteLine("Requesting download");
            SuSo.DownloadDataAsFile(data, FileName);
            Console.WriteLine("---- done ----");

            Console.ReadLine();
        }
    }


}
